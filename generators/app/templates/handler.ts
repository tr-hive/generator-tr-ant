/// <reference path="../typings/tsd.d.ts" />
import Rx = require("rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
var uuid = require("node-uuid");
import trAnt =require("tr-ant-utils");

const PKG_NAME = require("../package.json").name;

var newDate = () => new Date();  
var newUUID = () => uuid.v4();

export interface IHandlerOpts {
	handleStream: Rx.Observable<trAnt.INotif<any>>
	quotesStream: Rx.Observable<trAnt.IQuote[]>
	logger: logs.ILogger 	
	pub: rabbit.RabbitPub  
	newDate?() : Date
	newUUID?() : string  
}

export function handle(opts : IHandlerOpts) {
	if (opts.newDate)
		newDate = opts.newDate;
	if (opts.newUUID)		
		newUUID = opts.newUUID;
}



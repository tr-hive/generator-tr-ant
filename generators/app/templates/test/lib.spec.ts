/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/handler');
import rabbit = require('da-rabbitmq-rx');
import trAnt = require("tr-ant-utils");
import Rx = require("rx");
var expect = chai.expect;
import sinon = require("sinon");
import sinonChai = require("sinon-chai");

chai.use(sinonChai);

import getEnvVar = trAnt.getEnvVar;

const TEST_IDX = 5;
const RABBIT_URI = getEnvVar("RABBIT_URI_TEST"); 
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS") + TEST_IDX; 
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS") + TEST_IDX;

console.log(RABBIT_URI, RABBIT_QUEUE_NOTIFS, RABBIT_QUEUE_CMDS);

var expectedLogs = [];

var expectedCmd = {  };
	 
describe("check notifs are expired by the next day within two days",  () => {

	it("buyPercent:1, buyCancelPersnt:1 / NOTIF [SBER] - QUOTE [SBER:10] - DAY EXPIRED - NOTIF [SBER] - QUOTE [SBER:8] - DAY EXPIRED - one command",  (done) => {
		
		//publisher
		var pubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS};
		var pub = new rabbit.RabbitPub(pubOpts); 
		pub.connect();

		//subscribe here and validate all publishing
		var subOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_CMDS};
		var sub = new rabbit.RabbitSub(subOpts); 
		sub.connect();	
		
		var notif1 : trAnt.INotif<trAnt.INotifSilverSurferData> = {
			key : "1",
			issuer: "test",
			date: '2015-08-29T00:00:00.000Z',
			type: "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: 100,
				force: 1
			}						
		}
		
		var notif2 : trAnt.INotif<trAnt.INotifSilverSurferData> = {
			key : "2",
			issuer: "test",
			date: '2015-08-30T00:00:00.000Z',
			type: "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: 100,
				force: 1
			}						
		} 				 
		
		var quote1 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 10,
			bid: 10,
			ask: 10
		}
		
		var quote2 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 8,
			bid: 8,
			ask: 8
		}
		
		var logger  = {
			write(j) { console.log(JSON.stringify(j, null, 2)); }	
		};
				
		var logWrite = sinon.spy(logger, "write");
		var pubSpy = sinon.spy(pub, "write");
									
		sub.stream.take(1).subscribe(() => {
			
			//
			var notifsStream = Rx.Observable.create<trAnt.INotif<trAnt.INotifSilverSurferData>>(observer => {
				setTimeout(() => observer.onNext(notif1), 200);
				setTimeout(() => observer.onNext(notif2), 700);
			}).share();
			
			var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
				setTimeout(() => observer.onNext([quote1]), 300);
				setTimeout(() => observer.onNext([quote2]), 800);
			}).share();
								
			var buyOpts : handler.IBuyOpts =  {
				silverSurferStream: notifsStream,
				quotesStream: quotesStream, 
				buyPercent: 1,
				cancelPercent: 1,
				logger: {write : logWrite},
				pub: pub,
				startOfDayStream: Rx.Observable.timer(500, 1100),
				newDate() {return new Date(Date.UTC(2015, 0, 1));}
			};
						 
			handler.handleBuy(buyOpts);
		});

		setTimeout(() => { 						
			expect(logWrite).callCount(4);
			expect(pubSpy).calledOnce;
			expect(logWrite.firstCall.args[0]).eql(expectedLogs[0]);						
			expect(logWrite.secondCall.args[0]).eql(expectedLogs[1]);
			expect(logWrite.thirdCall.args[0]).eql(expectedLogs[2]);
			expect(logWrite.getCall(3).args[0]).eql(expectedLogs[3]);
			
			expect(pubSpy.firstCall.args[0]).eql(expectedCmd);
			
			done();
		}, 1500);			
	})
	
});		
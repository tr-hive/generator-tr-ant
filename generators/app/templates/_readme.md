# Trader micro service

## Project structure

+ /src - sources, type script  
  + index.ts - entry point of application
    
+ Dockerfile - docker image builder
+ .envs - define you config variables here (ignored in git)
+ typings - typescript definition files    
  
## Prerequisites

+ nodejs
+ npm
+ typescript


## Install

```
git clone git@bitbucket.org:tr-hive/<%= props.projName %>.git
npm install
```  
+ To rebuild `tsc` or `npm run-script build`
+ To watch and rebuild `tsc -w`

Rebuild also executed before each `npm start` 

## Start

+ Define enviroment variables, in order of priority `env`, `.npmrc`, `package.json`
+ Test start `npm start` 

## Docker 

+ Build container `docker build -t baio/<%= props.projName %> .`
+ Start container `docker run baio/<%= props.projName %>`

Also build `npm run-script docker` or `npm run-script docker-clean`  
 /// <reference path="../typings/tsd.d.ts" />
import rabbit = require("da-rabbitmq-rx");
import logs = require("da-logs");
import handler = require("./handler");
import trAnt = require("tr-ant-utils");
 
import getEnvVar = trAnt.getEnvVar;

const RABBIT_URI = getEnvVar("RABBIT_URI");
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFICATIONS");
const RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
const LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
const LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
const LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
const LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");

var logger = new logs.LoggerCompose({pack : <any>require("../package.json"), tags : []},  {
    loggly: {token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN},
    mongo: {connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION},
    console: true
 });


var pubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_CMDS};
var pub = new rabbit.RabbitPub(pubOpts); 
pub.connect();
pub.connectStream.subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: pubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", status : "error", err: err, opts: pubOpts});
	process.exit(1);
});

var subOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_QUOTES};
var sub = new rabbit.RabbitSub(subOpts); 
sub.connect();
sub.stream.take(1).subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: subOpts}) 
, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: subOpts});
	process.exit(1);
});

var handlerOpts : handler.IHandlerOpts = {
	handleStream: null,
	quotesStream: null,	
	logger: logger, 	
	pub: pub	
};

sub.stream.skip(1).subscribe((val) => {
 	logger.write({resource: "handle", oper: "start", status : "success", data: val}); 
	handler.handle(handlerOpts);	 
}	  
, (err) => {
	logger.write({resource: "rabbit", oper: "runtime", status : "error", err: err, opts: pubOpts});
	process.exit(1);
});
  

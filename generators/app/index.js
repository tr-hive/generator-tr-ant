'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var path = require('path');

module.exports = yeoman.generators.Base.extend({
  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the laudable ' + chalk.red('TrAnt') + ' generator!'
    ));

    var prompts = [ {
        name: 'appName',
        message: 'What your app\'s name??'
        }];

    this.prompt(prompts, function (props) {
      this.props = {
        projName : "tr-ant-" + props.appName,
        appName : props.appName
      };
      this.destinationRoot(path.join(this.destinationRoot(), '/' + this.props.projName));
      done();
    }.bind(this));
  },

  writing: {
    
    app: function () {
      this.template(
        this.templatePath('_package.json'),
        this.destinationPath('package.json')
      );
      this.template(
        this.templatePath('_readme.md'),
        this.destinationPath('readme.md')
      );            
    },

    projectfiles: function () {
      this.fs.copy(
        this.templatePath('index.ts'),
        this.destinationPath('src/index.ts')
      );
      this.fs.copy(
        this.templatePath('handler.ts'),
        this.destinationPath('src/handler.ts')
      );
      this.fs.copy(
        this.templatePath('test'),
        this.destinationPath('test')
      );                                          
      this.fs.copy(
        this.templatePath('editorconfig'),
        this.destinationPath('.editorconfig')
      );
      this.fs.copy(
        this.templatePath('.gitignore'),
        this.destinationPath('.gitignore')
      );
      this.fs.copy(
        this.templatePath('tsconfig.json'),
        this.destinationPath('tsconfig.json')
      );
      this.fs.copy(
        this.templatePath('.settings'),
        this.destinationPath('.settings')
      );
      this.fs.copy(
        this.templatePath('typings'),
        this.destinationPath('typings')
      );
      this.fs.copy(
        this.templatePath('.npmrc'),
        this.destinationPath('.npmrc')
      );      
      this.fs.copy(
        this.templatePath('Dockerfile'),
        this.destinationPath('Dockerfile')
      );            
    }
  },

  install: function () {
    this.npmInstall();
  }
});
